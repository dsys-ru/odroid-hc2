# ODROID HC2

## Install OS

Download Ubuntu image from https://wiki.odroid.com/odroid-xu4/os_images/linux/ubuntu_5.4/ubuntu_5.4

Installation instructions could be found here: https://wiki.odroid.com/getting_started/os_installation_guide

Unpack and flash the image to SD card:

```
wget https://odroid.in/ubuntu_20.04lts/XU3_XU4_MC1_HC1_HC2/ubuntu-20.04.1-5.4-minimal-odroid-xu4-20200812.img.xz
unxz ubuntu-20.04.1-5.4-minimal-odroid-xu4-20200812.img.xz
dd if=ubuntu-20.04.1-5.4-minimal-odroid-xu4-20200812.img of=/dev/<sdcard> bs=1M
```
Insert SD card into device, connect to your local network with Ethernet cable and power on.
The device will obtain its IP address via DHCP, you need to somehow determine the IP address of the device.
SSH to device as root using default credentials: `root`/`odroid`.

```
ssh root@<device-ip>

apt-get update
apt-get upgrade
```

## Install dependencies

Ubuntu comes with python3 preinstalled but we need to install other dependencies as well:

```
apt-get install -y python3-pip nginx liblapack-dev gfortran libxml2-dev libxslt-dev libjpeg-dev
pip3 install numpy scipy lxml xmltodict jsonpickle SimpleWebSocketServer obspy
```

## Format and mount HDD

Most likely you HDD will be identified as `/dev/sda` but run `fdisk -l` to make sure.

```
fdisk -l
```
If your HDD has a different device name -- replace `sda` with correct value in the following script:

```
mkfs.ext4 /dev/sda
tune2fs -U d88e9cdb-fa64-4e1b-bfa1-4b4b76329e2d /dev/sda
mkdir /mnt/apt
mount /dev/sda /mnt/apt
```

Note the output of the `blkid` command -- we use the UUID to automount the drive:

```
echo 'UUID=d88e9cdb-fa64-4e1b-bfa1-4b4b76329e2d /mnt/apt ext4 defaults 0 0\n' >> /etc/fstab
```

Make sure that `/etc/fstab` looks similar to:

```
UUID=e139ce78-9841-40fe-8823-96a304a09859 / ext4 errors=remount-ro,noatime 0 1
LABEL=boot /media/boot vfat defaults 0 1
UUID=d88e9cdb-fa64-4e1b-bfa1-4b4b76329e2d /mnt/apt ext4 defaults 0 0
```

To make sure that the drive in fact automounts:

```
umount /mnt/apt && mount /mnt/apt
```

## Install application

Create the application directories, note that they should be located on your HDD
rather than SD card:

### Frontent

```
mkdir -p /mnt/apt/frontend /mnt/apt/nginx
```

Build and deploy frontend:

```
cd ${APATIT}/frontent
yarn install
yarn build
scp -r dist/* root@<device-ip>:/mnt/apt/frontend/
```

Put `nginx/apatit.conf` to the device's `/mnt/apt/nginx` directory:

```
cd ${APATIT}/odroid-hc2
scp nginx/apatit.conf root@<device-ip>:/mnt/apt/nginx/
```

Configure nginx:

```
rm -f /etc/nginx/sites-enabled/default
ln -s /mnt/apt/nginx/apatit.conf /etc/nginx/conf.d/
ln -s /mnt/apt/frontend /var/www/apt
```

Test nginx settings:

```
nginx -t
```

This should result in:

```
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

Restart nginx

```
systemctl restart nginx
```

Make sure that you site is accessible at `http://<device-ip>`. Error message
`Failed to connect to backend` is expected at this point.

To (re)build and upgrade frontend (from your development machine):

```
cd apatit/frontend
git pull
yarn install
yarn build
cd dist
scp -r * root@<device-ip>:/mnt/apt/frontend/
```

### Backend

Create application directories:

```
mkdir -p /mnt/apt/backend /mnt/apt/backend/data
```

Deploy the backend:

```
cd ${APATIT}/backend
scp *.py root@<device-ip>:/mnt/apt/backend/
```

Create an empty device registry:

```
echo '[]' > /mnt/apt/backend/data/devices.json
```

Test the server by running:

```
cd /mnt/apt/backend
python3 ./server.py
```

Go back to browser and refresh page `http://<device-ip>/apt/`: there should be
no error message this time.

To make the backend run on reboot:

```
cd ${APATIT}/odroid-hc2
scp services/apatit.service root@<device-ip>:/lib/systemd/system/
```

Then on device:

```
cd /mnt/apt/backend
chown -R nobody data
systemctl daemon-reload
systemctl enable apatit.service
systemctl start apatit.service
```

To upgrade backend (from your development machine):

```
cd apatit/backend
git pull
scp *.py root@<device-ip>:/mnt/apt/backend/
```

Then on device:

```
systemctl restart apatit.service
```

### Patch OBSPY

OBSPY has a bug that prevents saving traces on ARM, so we need to patch the library:

```
cd ${APATIT}/odroid-hc2
scp -r patches root@<device-ip>:/tmp/
```

As we are running python 3.8:

```
cp /usr/local/lib/python3.8/dist-packages/obspy/io/mseed/headers.py /usr/local/lib/python3.8/dist-packages/obspy/io/mseed/headers.py.orig
patch /usr/local/lib/python3.8/dist-packages/obspy/io/mseed/headers.py /tmp/patches/headers.py.patch

cp /usr/local/lib/python3.8/dist-packages/obspy/io/mseed/core.py /usr/local/lib/python3.8/dist-packages/obspy/io/mseed/core.py.orig
patch /usr/local/lib/python3.8/dist-packages/obspy/io/mseed/core.py /tmp/patches/core.py.patch
```

Now the backend is ready and you may use your web browser to add a seismic device.

## Security

Change the `root` user password to something secure:

```
passwd
```

## Make SD read-only

This guide details on the benefits of readonly SD: https://spin.atomicobject.com/2015/03/10/protecting-ubuntu-root-filesystem/

Before changing SD to readonly mode, backup in case you will need to revert:

```
mount -f -o ro,remount /
dd if=/dev/mmcblk1 of=/mnt/apt/mmcblk1.img bs=512
```

Reboot then install and configure `overlayroot`:

```
apt-get install overlayroot
nano /etc/overlayroot.conf
```

The configuration file should be as follows:

```
overlayroot_cfgdisk="disabled"
overlayroot="tmpfs:swap=0,recurse=0"
```

Update initramfs (https://forum.odroid.com/viewtopic.php?t=20445):

```
update-initramfs -u
mkimage -A arm -O linux -T ramdisk -C none -a 0 -e 0 -n uInitrd -d /boot/initrd.img-$(uname -r) /media/boot/uInitrd
```

Reboot and you are done.

To chroot into the lower filesystem run:

```
overlayroot-chroot
```

## Debuging

Apatit service provides no logs, so to debug we need to run server.py in a screen console:

```
apt-get update
apt-get install -y screen
su nobody -s /bin/bash
screen
cd /mnt/apt/backend
python3 server.py
```

To reconnect to console:

```
su nobody -s /bin/bash
screen -ls
screen -r
```